import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys, os, time
import h5py
from glob import glob
from tqdm import tqdm
import h5py
from sklearn.preprocessing import MinMaxScaler
import argparse
from numba import jit

if sys.version[0] != '3':
    print("Runs on python3. exit")
    exit()


parser = argparse.ArgumentParser(description='make pdf for likelihood')
parser.add_argument('-fn', '--fn_input', action='store', type=str, required=True, help='input fn file name')
parser.add_argument('-me', '--me_input', action='store', type=str, required=True, help='input me file name')
parser.add_argument('-i', '--pdf_input', action='store', type=str, required=True, help='input pdf file name')
parser.add_argument('-o', '--csv_output', action='store', type=str, required=True, help='output csv file name')

PDFchannel = ['96','1']
parser.add_argument('--nCh_PDF', choices=PDFchannel, required=True, help='pdf channel to select')

parser.add_argument('-i_mean_var', '--input_m_v', action='store', type=str, required=True, help='mean var file name')

args = parser.parse_args()

file_check=0
exec('file_m_v = str("'+'./'+args.input_m_v+'")')
if os.path.isfile(file_m_v):
  print("Yes. it is a file")
  file_check+=1

nCh_pdf = int(args.nCh_PDF)


######### mean,var of each 248 bin
print('load var of train set')

if nCh_pdf==1 and file_check==0:
    fin0 = h5py.File('data_for_chi2.h5', 'r')
    list_fn_248 = fin0['events/fn_for_chi2']
    list_me_248 = fin0['events/me_for_chi2']
    nTT=len(list_fn_248)
    fn_mean_var=[]
    me_mean_var=[]
    fn_mean=[]
    me_mean=[]
    fn_var =[]
    me_var =[] 
    #@jit(nopython=True)
    for i in tqdm(range(nTT)):
        fn_248 = list_fn_248[i]
        me_248 = list_me_248[i]
        fn_mean_var.append([np.mean(fn_248),np.var(fn_248)])
        me_mean_var.append([np.mean(me_248),np.var(me_248)])
        fn_mean.append(np.mean(fn_248))
        fn_var.append(np.var(fn_248))
        me_mean.append(np.mean(me_248))
        me_var.append(np.var(me_248))
    fn_mean_var=np.array(fn_mean_var)
    me_mean_var=np.array(me_mean_var)
    plt.plot(fn_mean,label='FN',color='blue')
    plt.plot(me_mean,label='ME',color='red')    
    #plt.errorbar(np.arange(248), fn_mean, yerr=fn_var, ecolor='r', label='FN', linestyle="", color='k')#,capsize=1 )
    #plt.errorbar(np.arange(248), me_mean, yerr=me_var, ecolor='b', label='ME',  linestyle="", color='k')#,capsize=1  )
    plt.legend()

    """
        plt.errorbar(
        np.arange(248), # X
        me_mean, # Y
        me_var, # Y-errors
        fmt="ko-", # format line like for plot()
        linewidth=2, # width of plot line
        elinewidth=0.5,# width of error bar line
        ecolor='k', # color of error bar
        capsize=3, # cap length for error bar
        capthick=0.5 # cap thickness for error bar
        )
    """
    #plt.yscale('log')
    plt.show()
    plt.clf()




from numba import njit, prange
@njit(parallel=True)
def jit_ex(input_fn,input_me):
    fn_mean_var_96=[]#[] for i in range(96)]
    me_mean_var_96=[]#[] for i in range(96)]
    print('nTT: ',nTT)
    #@jit
    #@jit(nopython=True)
    for ch in range(96):
        fn_mv=[]
        me_mv=[]
        #print(ch,'th mean_var')
        for i in range(nTT):
            fn_248_96 = input_fn[ch][i]
            me_248_96 = input_me[ch][i]
            fn_mv.append([np.mean(fn_248_96),np.var(fn_248_96)])
            me_mv.append([np.mean(me_248_96),np.var(me_248_96)])
        fn_mean_var_96.append(fn_mv)
        me_mean_var_96.append(me_mv)

    return fn_mean_var_96, me_mean_var_96

if nCh_pdf==96 and file_check == 0:
    fn_var_96=[]
    me_var_96=[]
    fn_mean_96=[]
    me_mean_96=[]

    fin0 = h5py.File('data_for_chi2_96.h5', 'r')
    list_fn_248_96 = np.array(fin0['events/fn_for_chi2_96'])
    list_me_248_96 = np.array(fin0['events/me_for_chi2_96'])
    nTT=len(list_fn_248_96[0])
    """  
    fn_mean_var_96=[]#[] for i in range(96)]
    me_mean_var_96=[]#[] for i in range(96)]
    print('nTT: ',nTT)
    #@jit
    #@jit(nopython=True)
    for ch in range(96):
        fn_mv=[]
        me_mv=[]
        print(ch,'th mean_var')
        for i in tqdm(range(nTT)):
            fn_248_96 = list_fn_248_96[ch][i]
            me_248_96 = list_me_248_96[ch][i]
            fn_mv.append([np.mean(fn_248_96),np.var(fn_248_96)])
            me_mv.append([np.mean(me_248_96),np.var(me_248_96)])
        fn_mean_var_96.append(fn_mv)
        me_mean_var_96.append(me_mv)
    """
    fn_mean_var_96, me_mean_var_96 = jit_ex(list_fn_248_96,list_me_248_96)
    ########################
    fn_li=[[] for i in range(96)]
    me_li=[[] for i in range(96)]
    for ch in range(96):
        fn_mean=[]
        me_mean=[]
        fn_var =[]
        me_var =[]

        max_m_fn=[]
        max_m_me=[]
        for bin_ in range(248):
            max_m_fn.append(fn_mean_var_96[ch][bin_][0])
            max_m_me.append(me_mean_var_96[ch][bin_][0])
        n_fn = np.sum(max_m_fn)
        n_me = np.sum(max_m_me)
        for bin_ in range(248):
            fn_li[ch].append([(1/n_fn)*fn_mean_var_96[ch][bin_][0],(1/n_fn)*(1/n_fn)*fn_mean_var_96[ch][bin_][1]])
            me_li[ch].append([(1/n_me)*me_mean_var_96[ch][bin_][0],(1/n_me)*(1/n_me)*me_mean_var_96[ch][bin_][1]])
            fn_mean.append( (1/n_fn)*fn_mean_var_96[ch][bin_][0])
            fn_var.append(   (1/n_fn)*(1/n_fn)*fn_mean_var_96[ch][bin_][1])
            me_mean.append(  (1/n_me)*me_mean_var_96[ch][bin_][0] )
            me_var.append(  (1/n_me)*me_mean_var_96[ch][bin_][1] )


        #plt.plot(fn_mean,label='FN',color='blue')
        #plt.plot(me_mean,label='ME',color='red')    
        #plt.plot(np.sqrt(fn_var),label='FN',color='blue')
        #plt.plot(np.sqrt(me_var),label='ME',color='red')    



        #plt.errorbar(np.arange(248), fn_mean, fn_var,  elinewidth=1.5, ecolor='k', capsize=1, capthick=1.5,alpha=0.4 )
        #plt.errorbar(np.arange(248), me_mean, me_var,  elinewidth=1.5, ecolor='k', capsize=1, capthick=1.5,alpha=0.4 )
        #plt.errorbar(np.arange(248), fn_mean, yerr=fn_var, ecolor='r', label='FN', linestyle="", color='k')#,capsize=1 )
        #plt.errorbar(np.arange(248), me_mean, yerr=me_var, ecolor='b', label='ME',  linestyle="", color='k')#,capsize=1  )

        #plt.legend()
        #plt.xlim(50,100)
        #plt.show()
        #plt.pause(2)
        #plt.clf()



    fn_mean_var_96 = fn_li
    me_mean_var_96 = me_li
    #######################
 
    print(len(fn_mean_var_96[0]))
    print(np.sum([fn_mean_var_96[0][ii][0] for ii in range(248)]))
        #fn_v_96 = np.var(list_fn_248_96[ch])
        #me_v_96 = np.var(list_me_248_96[ch])
        #fn_var_96.append(fn_v_96)
        #me_var_96.append(me_v_96)
        #fn_m_96 = np.mean(list_fn_248_96[ch])
        #me_m_96 = np.mean(list_me_248_96[ch])
        #fn_mean_96.append(fn_m_96)
        #me_mean_96.append(me_m_96)


    kwargs = {'dtype':'f4', 'compression':'lzf'}
    print("Saving ch2...", end="")
    with h5py.File('mean_var_for_chi2.h5', 'w', libver='latest', swmr=True) as fout:
        g = fout.create_group('events')
        #g.create_dataset('waveforms', data=out_waveforms, chunks=(1, nCh*nT), **kwargs)
        g.create_dataset('fn_mv', data=fn_mean_var_96, chunks=(96,nTT,1), **kwargs)
        g.create_dataset('me_mv', data=me_mean_var_96, chunks=(96,nTT,1), **kwargs)
    print("Done.")



#print(fn_mean_var[400:500])
#print(me_mean_var[400:500])




####### make  square pdf

weight_z = pd.read_csv("pdf_z_weight_align_dV.csv")
weight_z_fn = np.array(weight_z['fn'])
weight_z_me = np.array(weight_z['me'])

if nCh_pdf==1:
    data = pd.read_csv(args.pdf_input)
    pdf_fn=data['fn']
    pdf_me=data['me']

    pdf_fn_bin=np.array(pdf_fn)
    pdf_me_bin=np.array(pdf_me)


if nCh_pdf==96:
    fin = h5py.File(args.pdf_input, 'r')
    pdf_fn_bin_96 = np.array(fin['events/fn_96pdf'])
    pdf_me_bin_96 = np.array(fin['events/me_96pdf'])



#figs,ax=plt.subplots(2)

###################   test
###############
###################################################likelihood now start
#figs,ax=plt.subplots(2)


#fin1 = h5py.File(args.fn_input, 'r')
#shape_1 = fin1['info/shape']
#fadcs_1 = fin1['events/waveforms']

#fin2 = h5py.File(args.me_input, 'r')
#shape_2 = fin2['info/shape']
#fadcs_2 = fin2['events/waveforms']

data = pd.read_csv("classify_fadc_align_dV.csv")
n_train_fn=np.array(data["train_fn"])
n_train_me=np.array(data["train_me"])
n_test_fn=np.array(data["test_fn"])
n_test_me=np.array(data["test_me"])

n_train_fn=np.delete(n_train_fn,np.where((n_train_fn==-1)))
n_train_me=np.delete(n_train_me,np.where((n_train_me==-1)))
n_test_fn=np.delete(n_test_fn,np.where((n_test_fn==-1)))
n_test_me=np.delete(n_test_me,np.where((n_test_me==-1)))


#################just1
chi2_fn_likefnL=[]
chi2_fn_likemeL=[]
chi2_me_likefnL=[]
chi2_me_likemeL=[]

chi2_fn_likefnL_96=[]
chi2_fn_likemeL_96=[]
chi2_me_likefnL_96=[]
chi2_me_likemeL_96=[]

diff_fn=[]
diff_me=[]


#import math as

###########

#@jit(nopython=True)
@njit #(parallel=True)
def chi2_cal_248(fadc,mv_each_ch,max_,max_fadc):
    #fadc_ch=(max_/max_fadc)*fadc
    fadc_ch=fadc#*(1/np.sum(fadc))
    chi2_248=0
    for k in range(nT):
        if mv_each_ch[k][1]!=0:              
            chi2_248+=(fadc_ch[k]-mv_each_ch[k][0])*(fadc_ch[k]-mv_each_ch[k][0])*(1/mv_each_ch[k][1])
    return chi2_248
################################################  FN ################################
xx=np.array([i for i in range(nTT)])
print(type(args.fn_input))
fin = h5py.File(args.fn_input, 'r')
shape = np.array(fin['info/shape'])
fadcs = fin['events/waveform']
trigID = fin['events/trigID']
subrun = fin['events/subrun']
run_fn = fin['events/run']
out_run_FN=[]
out_subrun_FN=[]
out_trigID_FN=[]


nT=int(shape[-1])
nCh=int(shape[0])

#figs, ax = plt.subplots(2)

print(len(n_test_fn))
br=0
#re_fadc=[]
sum_re_fadc=[]
c96_re_fadc=[]
for i in tqdm(n_test_fn): #range(len(fadcs))):
    i=int(i)

    out_run_FN.append(int(run_fn[i]))
    out_subrun_FN.append(int(subrun[i]))
    out_trigID_FN.append(int(trigID[i]))   

    re_fadc=[]
    fadc = fadcs[i]
    #fadc = fadc.reshape(96,248)
    fadc = fadc.reshape(shape)
    for j in range(nCh):
        count0 =0 
        fadc_window=fadc[j]
        #count0 = int(fadc_window.tolist().count(0))
        if count0>-1:#count_mean:
            fadc_ = np.array(fadc[j])
            #if abs(np.sum(fadc_)-0)<1:
            #    print(np.sum(fadc_))
            if np.sum(fadc_)!=0 and nCh_pdf ==96:
                fadc_ = (1/np.sum(fadc_))*fadc_
            re_fadc.append(fadc_)
    re_fadc=np.array(re_fadc)

    if nCh_pdf==1:
        re_fadc_sum=np.zeros(nT)
        for ch in range(nCh):
            re_fadc_sum+=re_fadc[ch]
        re_fadc_sum=(1/np.sum(re_fadc_sum))*re_fadc_sum
        re_fadc_sum = re_fadc_sum#*(1/weight_z_fn[i])
        #re_fadc_sum = (1/np.sum(re_fadc_sum))*re_fadc_sum
        #sum_re_fadc.append(re_fadc_sum)

        #re_fadc_sum_1 = np.array(re_fadc_sum)
        
        fnfn=0
        fnme=0

        max_fadc=1
        if np.max(re_fadc_sum)!=0:
            max_fadc=np.max(re_fadc_sum)
        max_fn=np.max([fn_mean_var[k][0] for k in range(248)])
        re_fadc_sum=(max_fn/max_fadc)*re_fadc_sum

        chi2_fn_likefn=0
        for k in range(nT):
            chi2_fn_likefn+=(re_fadc_sum[k]-fn_mean_var[k][0])*(re_fadc_sum[k]-fn_mean_var[k][0])#*(1/fn_mean_var[k][1])
        chi2_fn_likeme=0
        for k in range(nT):
            chi2_fn_likeme+=(re_fadc_sum[k]-me_mean_var[k][0])*(re_fadc_sum[k]-me_mean_var[k][0])#*(1/me_mean_var[k][1])
        fnfn+=chi2_fn_likefn/248#*1e7
        fnme+=chi2_fn_likeme/248#*1e7
        chi2_fn_likefnL.append(fnfn)
        chi2_fn_likemeL.append(fnme)

        diff_fn.append(fnme-fnfn)


    if nCh_pdf ==96:
        c96_re_fadc.append(re_fadc)
        
        fnfn=0
        fnme=0
        #re_fadc_sum_1 = np.array(re_fadc_sum)
        for ch in range(nCh):
            max_fadc=1
            if np.max(re_fadc[ch])!=0:
                max_fadc=np.max(re_fadc[ch])
            max_fn=np.max([fn_mean_var_96[ch][k][0] for k in range(248)])
            chi2_fn_likefn = chi2_cal_248(np.array(re_fadc[ch]),np.array(fn_mean_var_96[ch]),max_fn,max_fadc)
            """
            chi2_fn_likefn=0
            for k in range(nT):
                 if fn_mean_var_96[ch][k][1]!=0:              
                    chi2_fn_likefn+=(re_fadc[ch][k]-fn_mean_var_96[ch][k][0])*(re_fadc[ch][k]-fn_mean_var_96[ch][k][0])*(1/fn_mean_var_96[ch][k][1])
                #print(re_fadc[ch][k]-pdf_fn_bin_96[ch][k])
                #print('var: ',fn_mean_var_96[ch][k][1])
            """

            #print('chi2_fn_likefn',chi2_fn_likefn)
            max_me=np.max([me_mean_var_96[ch][k][0] for k in range(248)])
            chi2_fn_likeme = chi2_cal_248(np.array(re_fadc[ch]),np.array(me_mean_var_96[ch]),max_me,max_fadc)
            """
            chi2_fn_likeme=0
            for k in range(nT):
                if me_mean_var_96[ch][k][1]!=0:
                    chi2_fn_likeme+=(re_fadc[ch][k]-me_mean_var_96[ch][k][0])*(re_fadc[ch][k]-me_mean_var_96[ch][k][0])*(1/me_mean_var_96[ch][k][1])
            """
            fnfn+=chi2_fn_likefn/(248*nCh_pdf)#*1e6
            fnme+=chi2_fn_likeme/(248*nCh_pdf)#*1e6
            
        #fnfn,fnme = chi2_cal_fn(re_fadc,fn_mean_var_96,me_mean_var_96)

        chi2_fn_likefnL_96.append(fnfn)
        chi2_fn_likemeL_96.append(fnme)
        diff_fn.append(fnme-fnfn)

    #br+=1
    #if br>50:
    #    break

    """
    likeli_1=-1*np.dot(re_fadc_sum_1,np.log(pdf_fn_bin))
    likeli_2=-1*np.dot(re_fadc_sum_1,np.log(pdf_me_bin))
  
    #print(likeli_1)
    if (np.exp(-likeli_1)+np.exp(-likeli_2)) ==0:
        likeli_a=-10
    else:  
        likeli_a=np.exp(-likeli_2)/(np.exp(-likeli_1)+np.exp(-likeli_2))
    likeli_a__=likeli_2/(likeli_1+likeli_2)
    #print(likeli_a)


    like_hist_1.append(likeli_a)
    like_hist_1__.append(likeli_a__)

    """



#######################################################################         ME       #######################################################
fin2 = h5py.File(args.me_input, 'r')
shape = np.array(fin2['info/shape'])
fadcs = fin2['events/waveform']
trigID = fin2['events/trigID']
subrun = fin2['events/subrun']
run_me = fin2['events/run']


out_run_ME=[]
out_subrun_ME=[]
out_trigID_ME=[]



nT=int(shape[-1])
nCh=int(shape[0])

#br=0
print(len(n_test_me))
sum_re_fadc=[]
c96_re_fadc=[]
for i in tqdm(n_test_me): #range(len(fadcs))):
    i=int(i)

    out_run_ME.append(int(run_me[i]))
    out_subrun_ME.append(int(subrun[i]))
    out_trigID_ME.append(int(trigID[i]))  

    re_fadc=[]
    fadc = fadcs[i]
    #fadc = fadc.reshape(96,248)
    fadc = fadc.reshape(shape)
    #count_mean=0
    #for j in range(nCh):
    #    fadc_window=fadc[j]
    #    count_mean += int(fadc_window.tolist().count(0.))
    #count_mean = int(count_mean/nCh)


    for j in range(nCh):
        count0 =0 
        fadc_window=fadc[j]
        #count0 = int(fadc_window.tolist().count(0))
        if count0>-1:#count_mean:
            fadc_ = np.array(fadc[j])
            if np.sum(fadc_)!=0 and nCh_pdf==96:
                fadc_ = (1/np.sum(fadc_))*fadc_
            re_fadc.append(fadc_)
    re_fadc=np.array(re_fadc)
    #re_fadc=(1/np.sum(re_fadc))*re_fadc

    #print(len(re_fadc))
    if nCh_pdf==1:
        re_fadc_sum=np.zeros(nT)
        for ch in range(nCh):
            re_fadc_sum+=re_fadc[ch]
        re_fadc_sum=(1/np.sum(re_fadc_sum))*re_fadc_sum
        re_fadc_sum = re_fadc_sum#*(1/weight_z_fn[i])
        #re_fadc_sum = (1/np.sum(re_fadc_sum))*re_fadc_sum
        #sum_re_fadc.append(re_fadc_sum)

        #re_fadc_sum_1 = np.array(re_fadc_sum)
        mefn=0
        meme=0

        max_fadc=1
        if np.max(re_fadc_sum)!=0:
            max_fadc=np.max(re_fadc_sum)
        max_me=np.max([me_mean_var[k][0] for k in range(248)])
        re_fadc_sum=(max_me/max_fadc)*re_fadc_sum


        chi2_me_likefn=0
        for k in range(nT):
            chi2_me_likefn+=(re_fadc_sum[k]-fn_mean_var[k][0])*(re_fadc_sum[k]-fn_mean_var[k][0])#*(1/fn_mean_var[k][1])
            #print(re_fadc_sum[k]-pdf_fn_bin[k])
            #print('var: ',fn_mean_var[k][1])

        #print('chi2_me_likefn',chi2_me_likefn)
        chi2_me_likeme=0
        for k in range(nT):
            chi2_me_likeme+=(re_fadc_sum[k]-me_mean_var[k][0])*(re_fadc_sum[k]-me_mean_var[k][0])#*(1/me_mean_var[k][1])
        mefn+=chi2_me_likefn/248#*1e7
        meme+=chi2_me_likeme/248#*1e7
        chi2_me_likefnL.append(mefn)
        chi2_me_likemeL.append(meme)

        diff_me.append(meme-mefn)


    if nCh_pdf ==96:
        c96_re_fadc.append(re_fadc)
        mefn=0
        meme=0
        #re_fadc_sum_1 = np.array(re_fadc_sum)
        for ch in range(nCh):
            max_fadc=1
            if np.max(re_fadc[ch])!=0:
                max_fadc=np.max(re_fadc[ch])
            max_me=np.max([me_mean_var_96[ch][k][0] for k in range(248)])
            chi2_me_likefn = chi2_cal_248(np.array(re_fadc[ch]),np.array(fn_mean_var_96[ch]),max_me,max_fadc)
            """
            chi2_me_likefn=0
            for k in range(nT):
                if fn_mean_var_96[ch][k][1]!=0:
                    chi2_me_likefn+=(re_fadc[ch][k]-fn_mean_var_96[ch][k][0])*(re_fadc[ch][k]-fn_mean_var_96[ch][k][0])*(1/fn_mean_var_96[ch][k][1])
                #print(re_fadc[ch][k]-fn_mean_var_96[ch][k][0])
                #print('var: ',fn_mean_var_96[ch][k][1])
            """
            #print('chi2_me_likefn',chi2_me_likefn)
            max_me=np.max([me_mean_var_96[ch][k][0] for k in range(248)])
            chi2_me_likeme = chi2_cal_248(np.array(re_fadc[ch]),np.array(me_mean_var_96[ch]),max_me,max_fadc)
            """
            chi2_me_likeme=0
            for k in range(nT):
                if me_mean_var_96[ch][k][1]!=0:
                    chi2_me_likeme+=(re_fadc[ch][k]-me_mean_var_96[ch][k][0])*(re_fadc[ch][k]-me_mean_var_96[ch][k][0])*(1/me_mean_var_96[ch][k][1])
            #chi2_me_likefnL_96.append(chi2_me_likefn)#/nT)
            #chi2_me_likemeL_96.append(chi2_me_likeme)#/nT)
            """


            mefn+=chi2_me_likefn/(248*nCh_pdf)#*1e6
            meme+=chi2_me_likeme/(248*nCh_pdf)#*1e6
        chi2_me_likefnL_96.append(mefn)
        chi2_me_likemeL_96.append(meme)
        diff_me.append(meme-mefn)


    #br+=1
    #if br>50:
    #    break

    """
    likeli_3=-1*np.dot(re_fadc_sum_2,np.log(pdf_fn_bin))
    likeli_4=-1*np.dot(re_fadc_sum_2,np.log(pdf_me_bin))
    #print(pdf_fn_bin[0])
    #print(re_fadc_sum_2[0])
    if (np.exp(-likeli_3)+np.exp(-likeli_4)) ==0:
        likeli_b=0
    else:
        likeli_b=np.exp(-likeli_4)/(np.exp(-likeli_3)+np.exp(-likeli_4))
    #print(-likeli_3)
    likeli_b__=likeli_4/(likeli_3+likeli_4)
    #print(likeli_b)


    like_hist_2.append(likeli_b)
    like_hist_2__.append(likeli_b__)
    """




################################################################################################################################





    #print(len(re_fadc))

    #re_fadc_sum_1=[]
    #re_fadc_sum_2=[]

"""
    re_fadc_sum_1 = np.array(re_fadc_sum_1[35:])
    re_fadc_sum_2 = np.array(re_fadc_sum_2[35:])



    likeli_1=-1*np.dot(re_fadc_sum_1,np.log(pdf_fn_bin))
    likeli_2=-1*np.dot(re_fadc_sum_1,np.log(pdf_me_bin))

    likeli_3=-1*np.dot(re_fadc_sum_2,np.log(pdf_fn_bin))
    likeli_4=-1*np.dot(re_fadc_sum_2,np.log(pdf_me_bin))



    likeli_a=likeli_1/(likeli_1+likeli_2)
    print(likeli_a)
    likeli_b=likeli_3/(likeli_3+likeli_4)
    print(likeli_b)


    like_hist_1.append(likeli_a)
    like_hist_2.append(likeli_b)
"""

#print(chi2_fn_likemeL)
#print(chi2_me_likemeL)

#matplotlib.pyplot.hist2d(x, y, bins=10, range=None, density=False, weights=None, cmin=None, cmax=None, *, data=None, **kwargs)
#ax[0].hist(like_hist_1,range=(0.499,0.501),bins=100,color = 'blue',alpha=0.4)
#ax[0].hist(like_hist_2,range=(0.499,0.501),bins=100,color = 'red',alpha=0.4)
#plt.hist(like_hist_1,label='FN',range=(0.,0.65),bins=150,color = 'blue',alpha=0.4,density=True)
#plt.hist(like_hist_2,label='ME',range=(0.,0.65),bins=150,color = 'red',alpha=0.4,density=True )
#figs, ax = plt.subplots(1,2)

plt.title("Chi2 Method", fontsize=15)
if nCh_pdf==1:
    plt.scatter(chi2_fn_likefnL, chi2_fn_likemeL,label='FN', color='blue', alpha=.3)
    plt.scatter(chi2_me_likefnL, chi2_me_likemeL,label='ME', color='red', alpha=.3)
if nCh_pdf==96:
    plt.scatter(chi2_fn_likefnL_96, chi2_fn_likemeL_96,label='FN', color='blue', alpha=.3)
    plt.scatter(chi2_me_likefnL_96, chi2_me_likemeL_96,label='ME', color='red', alpha=.3)

plt.xlim(0, 10000)
plt.ylim(0, 10000)
plt.grid()
plt.legend()
plt.xlabel("chi2 like FN", fontsize=13)
plt.ylabel("chi2 like ME", fontsize=13)
#plt.xscale('log')
#plt.yscale('log')

plt.show()



print('diff_fn_length: ',len(diff_fn))
print('diff_me_length: ',len(diff_me))
print(len(chi2_me_likefnL_96))


#plt.hist2d(chi2_fn_likefn,chi2_fn_likeme,label='FN', bins=100, color='blue',alpha=0.4)
#plt.hist2d(chi2_me_likefn,chi2_me_likeme,label='ME',  bins=100, color='red',alpha=0.4)
if nCh_pdf==1:
    pd2 = pd.DataFrame({'run': out_run_FN, 'subrun': out_subrun_FN, 'trigID': out_trigID_FN,'Diff_FN':diff_fn,'fn_l_fn': chi2_fn_likefnL , 'fn_l_me':  chi2_fn_likemeL  })
    pd2.to_csv('FN'+args.csv_output)
    pd3 = pd.DataFrame({'run': out_run_ME, 'subrun': out_subrun_ME, 'trigID': out_trigID_ME,'Diff_ME': diff_me, 'me_l_fn': chi2_me_likefnL , 'me_l_me': chi2_me_likemeL })
    pd3.to_csv('ME'+args.csv_output)
 
    chi2_fn_likefnL = np.hstack([np.array(chi2_fn_likefnL), -1*np.ones(len( chi2_me_likefnL )-len( chi2_fn_likefnL ))])
    chi2_fn_likemeL = np.hstack([np.array(chi2_fn_likemeL), -1*np.ones(len( chi2_me_likemeL )-len( chi2_fn_likemeL ))])
    diff_fn = np.hstack([np.array(diff_fn),-1e33*np.ones(len(diff_me)-len(diff_fn))])
    pd1 = pd.DataFrame({'Diff_FN':diff_fn,'Diff_ME': diff_me, 'fn_l_fn': chi2_fn_likefnL , 'fn_l_me':  chi2_fn_likemeL , 'me_l_fn': chi2_me_likefnL , 'me_l_me': chi2_me_likemeL })
    pd1.to_csv(args.csv_output)
  
    
if nCh_pdf==96:
    pd2 = pd.DataFrame({'run': out_run_FN, 'subrun': out_subrun_FN, 'trigID': out_trigID_FN,'Diff_FN':diff_fn,'fn_l_fn': chi2_fn_likefnL_96 , 'fn_l_me':  chi2_fn_likemeL_96  })
    pd2.to_csv('FN'+args.csv_output)
    pd3 = pd.DataFrame({'run': out_run_ME, 'subrun': out_subrun_ME, 'trigID': out_trigID_ME,'Diff_ME': diff_me, 'me_l_fn': chi2_me_likefnL_96 , 'me_l_me': chi2_me_likemeL_96 })
    pd3.to_csv('ME'+args.csv_output)
 
    chi2_fn_likefnL_96 = np.hstack([np.array(chi2_fn_likefnL_96), -1*np.ones(len( chi2_me_likefnL_96 )-len( chi2_fn_likefnL_96 ))])
    chi2_fn_likemeL_96 = np.hstack([np.array(chi2_fn_likemeL_96), -1*np.ones(len( chi2_me_likemeL_96 )-len( chi2_fn_likemeL_96 ))])
    diff_fn = np.hstack([np.array(diff_fn),-1e33*np.ones(len(diff_me)-len(diff_fn))])
    pd1 = pd.DataFrame({'Diff_FN':diff_fn,'Diff_ME': diff_me,'fn_l_fn': chi2_fn_likefnL_96 , 'fn_l_me':  chi2_fn_likemeL_96 , 'me_l_fn': chi2_me_likefnL_96 , 'me_l_me': chi2_me_likemeL_96 })
    pd1.to_csv(args.csv_output)


#plt.pause(1)

