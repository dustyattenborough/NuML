#!/usr/bin/env python
import sys, os
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
flags = ["ME","FN"]
parser.add_argument('--flag', choices=flags, required=True, help='label name to select')
parser.add_argument('--cutZ', action='store', default=0.6, type=float, help='fiducial cut |z|')
parser.add_argument('--cutR', action='store', default=0.6, type=float, help='fiducial cut sqrt(x^2+y^2)')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='Supress verbose output')
args = parser.parse_args()

from glob import glob
fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

nT = 0
import uproot
import numpy as np
import tqdm
subRuns, trigIDs = [], []
fadcs_lo, fadcs_hi, fadcs_veto = [], [], []
for fName in (fNames if args.quiet else tqdm.tqdm(fNames)):
    fin = uproot.open(fName)
    tree = fin['prod']

    ## Event selection by the tagging information, ME or FN
    isTagged = np.array(tree[args.flag+'Flag'].array())
    for flagName in flagNames:
        if flagName == args.flag: continue
        isTagged = isTagged & ~np.array(tree[flagName+'Flag'].array())

    ## Event selection by the fiducial volume cut
    vtx3 = np.array(tree['recoVertexArray'].array())
    vtxR = np.hypot(vtx3[:,0], vtx3[:,1])
    vtxZ = vtx3[:,2]
    isFiducial = (vtxR<args.cutR) & (np.abs(vtxZ)<args.cutZ)

    fadc = np.array(tree['FADC'].array())
    if nT == 0: nT = fadc.shape[-1]
    if fadc.shape[1:] != (28, 8, nT):
        print("!!! Inconsistent array shape, ", fadc.shape)
        break

    # split FADC by board/channels, 8 channels per boards
    fadc_hi = fadc[:, 1: 1+12].reshape([-1,96,nT]) ## High gain channels: 1-12
    fadc_lo = fadc[:,13:13+12].reshape([-1,96,nT]) ## Low gain channels: 13-24
    fadc_veto = fadc[:,25:25+ 3].reshape([-1, 3*8,nT]) ## veto channels: 25,26,27

    fadcs_lo.append(fadc_lo)
    fadcs_hi.append(fadc_hi)
    fadcs_veto.append(fadc_veto)

    subRun = 0 if 'debug.r' not in fName else fName.split('.')[-2][1:].lstrip('0')
    subRun = 0 if subRun == '' else int(subRun)
    subRuns.append(np.ones(fadc.shape[0])*subRun)
    trigIDs.append(tree['TrigID'].array())

## shape: (nEvent, nPMT, nTime)
fadcs_lo = np.concatenate(fadcs_lo, axis=0)
fadcs_hi = np.concatenate(fadcs_hi, axis=0)
fadcs_veto = np.concatenate(fadcs_veto, axis=0)
subRuns = np.concatenate(subRuns)
trigIDs = np.concatenate(trigIDs)

if not args.quiet:
    print("Low gain channel : shape=", fadcs_lo.shape)
    print("                     min=", fadcs_lo.min(), "max=", fadcs_lo.max())
    print("High gain channel: shape=", fadcs_hi.shape)
    print("                     min=", fadcs_hi.min(), "max=", fadcs_hi.max())
    print("Veto channel     : shape=", fadcs_veto.shape)
    print("                     min=", fadcs_veto.min(), "max=", fadcs_veto.max())

import matplotlib.pyplot as plt

for i in range(fadcs_lo.shape[0]):
    fig, axs = plt.subplots(13,8, figsize=(11.5,8.1))
    axs[0,0].text(0,0.6,'Run    =...')
    axs[0,0].text(0,0.3,'Subrun =%d' % subRuns[i])
    axs[0,0].text(0,0.0,'TrigId =%d' % trigIDs[i])
    for c in range(8):
        axs[0,c].axes.xaxis.set_visible(False)
        axs[0,c].axes.yaxis.set_visible(False)
    for p in range(12):
        for c in range(8):
            k = p*8+c

            ## Compute pedestal
            pdstl_lo = np.mean(fadcs_lo[i,k,:35])
            pdstl_hi = np.mean(fadcs_hi[i,k,:35])

            ## x values and y values, scaled by each PMT channel's gains
            t = np.linspace(0, nT*2, nT)
            y_lo = -16.*(fadcs_lo[i,k]-pdstl_lo)
            y_hi = -0.6*(fadcs_hi[i,k]-pdstl_hi)

            ## Draw guidelines
            axs[p+1,c].plot([0, nT*2], [0, 0], linewidth=.7, color='tab:gray')
            axs[p+1,c].plot([70,70], [min(y_lo.min(), y_hi.min()), max(y_lo.max(), y_hi.max())], linewidth=.7, color='tab:gray')

            ## Change colors if the pulse is saturated
            isSaturate_hi = (np.count_nonzero(fadcs_hi[i,k]) < nT-1)
            lc_hi = 'r' if isSaturate_hi else 'tab:orange'
            isSaturate_lo = (np.count_nonzero(fadcs_lo[i,k]) < nT-1)
            lc_lo = 'b' if isSaturate_lo else 'tab:blue'

            ## Draw the lineshapes
            axs[p+1,c].plot(t, y_lo, drawstyle='steps-pre', linewidth=1., color=lc_lo)
            axs[p+1,c].plot(t, y_hi, drawstyle='steps-pre', linewidth=1., color=lc_hi)

            axs[p+1,c].axes.xaxis.set_visible(False)
            axs[p+1,c].axes.yaxis.set_visible(False)
    plt.tight_layout()
    plt.show()
