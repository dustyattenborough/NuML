#!/usr/bin/env python
import sys, os, argparse, uproot
import numpy as np, pandas as pd, xgboost as xgb

fin = uproot.open(sys.argv[1])
fout = sys.argv[2]

varNames = []
df = pd.DataFrame()

df['labels'] = np.array(fin["tree/label"].array().tolist())
for i in range(2480//(5*16)):
    varName = 'wf%d'%i
    varNames.append(varName)
    df[varName] = np.array(fin["tree/"+varName].array().tolist())

trnDataset = df.sample(frac=0.6, random_state=12345)
valDataset = df.drop(trnDataset.index)
testDataset = valDataset.sample(frac=0.5, random_state=12345)
valDataset = valDataset.drop(testDataset.index)

trnDataset = xgb.DMatrix(data=trnDataset[varNames], label=trnDataset['labels'])
valDataset = xgb.DMatrix(data=valDataset[varNames], label=valDataset['labels'])
#testDataset = xgb.DMatrix(data=testDataset[varNames], label=testDataset['labels'])

params = {'max_depth' : 3,
         'eta' : 0.1, 
         'objective' : 'binary:logistic',
         'eval_metric' : 'logloss',
         'early_stoppings' : 500 }
num_rounds = 400
wlist = [(trnDataset, 'trn'), (valDataset, 'val')]
#model = xgb.XGBClassifier()
#model.fit(trnDataset[varNames], trnDataset['labels'])
model = xgb.train(params=params, dtrain=trnDataset,
                  num_boost_round=num_rounds, evals=wlist)
model.save_model(fout)

