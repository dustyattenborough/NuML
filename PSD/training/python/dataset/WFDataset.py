#!/usr/bin/env python
import numpy as np
import h5py
import torch
from torch.utils.data import Dataset

class WFDataset(Dataset):
    def __init__(self, fin, **kwargs):
        super(WFDataset, self).__init__()

        self.fin = h5py.File(fin, "r", libver='latest')
        self.wfs = self.fin['events/waveforms']
        self.labels = self.fin['events/labels']

        self.nCh = kwargs['channel']
        self.nPt = self.wfs.shape[1]//self.nCh

        self.nEvents = len(self.labels)

        ## Assign weights for the events with label==1
        ## For the label==0, we set weight=1
        self.weights = np.ones(self.nEvents)
        nEvents1 = np.count_nonzero(self.labels)
        nEvents0 = self.nEvents - nEvents1
        w = nEvents0/nEvents1
        #self.weights[self.labels[()]==0] = w
        self.classWeight = torch.FloatTensor([w])
        #print("clsassWeight=", weight)

    def __getitem__(self, idx):
        wf = torch.FloatTensor(self.wfs[idx])
        wf /= wf.max()
        wf = wf.view(self.nCh, self.nPt)
        label = torch.FloatTensor([self.labels[idx]])
        return (wf, label)#, self.weights[idx])

    def __len__(self):
        return self.nEvents

