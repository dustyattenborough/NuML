from models.WF1DCNN1FC1Model import *
from models.WF1DCNN1FC2Model import *

from models.WF1DCNN3FC1Model import *
from models.WF1DCNN3FC2Model import *

from models.WF1DCNN5FC1Model import *
from models.WF1DCNN5FC2Model import *

from models.WF2DCNN1FC1Model import *
from models.WF2DCNN1FC2Model import *

from models.WF2DCNN3FC1Model import *
from models.WF2DCNN3FC2Model import *

from models.WF2DCNN5FC1Model import *
from models.WF2DCNN5FC2Model import *

from models.WFLSTMModel import *
