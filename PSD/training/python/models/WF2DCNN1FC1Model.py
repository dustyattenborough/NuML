#!/usr/bin/env python
import torch.nn as nn

class WF2DCNN1FC1Model(nn.Module):
    def __init__(self, **kwargs):
        super(WF2DCNN1FC1Model, self).__init__()

        self.nPt = kwargs['nPoint']
        self.nCh = 1 if 'nChannel' not in kwargs else kwargs['nChannel']

        nPt = self.nPt
        nH = 2
        kernel1 = 8 if 'kernel_size' not in kwargs else kwargs['kernel_size']

        self.conv1 = nn.Sequential(
            nn.Conv2d(self.nCh//2, 64, kernel_size=(2,kernel1), padding=(1,0)),
            nn.MaxPool2d((1,kernel1)),
            nn.ReLU(),
            nn.BatchNorm2d(64),
            nn.Dropout(0.5),
        )
        nPt = (nPt-kernel1+1)//kernel1
        nH += 1

        nPt = nPt*nH

        self.fc = nn.Sequential(
            nn.Linear(nPt*64, 512),
            nn.ReLU(), nn.Dropout(0.5),

            nn.Linear(512, 1),
        )

    def forward(self, x):
        batch, c, n = x.shape
        #assert (n == self.nPt)
        #assert (c == self.nCh)

        x = x.view(-1,c//2,2,n)
        x = self.conv1(x)

        x = x.flatten(start_dim=1)
        x = self.fc(x)

        return x

