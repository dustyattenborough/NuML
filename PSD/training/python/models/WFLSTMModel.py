#!/usr/bin/env python
import torch.nn as nn
import torch

class WFLSTMModel(nn.Module):
    def __init__(self, **kwargs):
        super(WFLSTMModel, self).__init__()

        nPt = kwargs['nPoint']
        nCh = 1 if 'channel' not in kwargs else kwargs['channel']
        self.nStates = 64 if 'nHiddenStates' not in kwargs else kwargs['nHiddenStates']
        nLayers = 1 if 'nHiddenLayers' not in kwargs else kwargs['nHiddenLayers']

        self.lstm = nn.GRU(input_size=nCh, hidden_size=self.nStates, num_layers=nLayers,
                            batch_first=True, bidirectional=True)

        self.fc = nn.Sequential(
            nn.Linear(self.nStates*2, 1),
            #nn.Linear(self.nStates*2, 512),
            #nn.ReLU(), nn.Dropout(0.5),

            #nn.Linear(512, 1),
        )

    def forward(self, x):
        batch, nCh, nPt = x.shape
        x = x.transpose(2,1) ## we need batch, nPt, nCh
        x, _ = self.lstm(x)
        x = x[:,-1,:].flatten(start_dim=1)
        x = self.fc(x)

        return x

