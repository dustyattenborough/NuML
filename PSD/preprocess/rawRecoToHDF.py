#!/usr/bin/env python
import sys, os
import argparse
if sys.version[0] != '3':
    print("Runs on python3. exit")
    exit()

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='INPUT_FILE_WITH_FADC.root', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='output file name')
flagNames = ["ME","FN"]
parser.add_argument('--flag', choices=flagNames, required=True, help='label name to select')
parser.add_argument('--cutZ', action='store', default=0.6, type=float, help='fiducial cut |z|')
parser.add_argument('--cutR', action='store', default=0.6, type=float, help='fiducial cut sqrt(x^2+y^2)')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='Supress verbose output')
args = parser.parse_args()

from glob import glob
fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

import uproot
import numpy as np
import tqdm
nT = 0
out_labels = []
out_fadcs_lo, out_fadcs_hi = [], []
out_subRuns, out_trigIDs = [], []
out_vtxRs, out_vtxZs = [], []
#out_dR, out_dT = [], []
for fName in (fNames if args.quiet else tqdm.tqdm(fNames)):
    fin = uproot.open(fName)
    tree = fin['prod']

    ## Event selection by the tagging information, ME or FN
    isTagged = np.array(tree[args.flag+'Flag'].array())
    for flagName in flagNames:
        if flagName == args.flag: continue
        isTagged = isTagged & ~np.array(tree[flagName+'Flag'].array())

    ## Event selection by the fiducial volume cut
    vtx3 = np.array(tree['recoVertexArray'].array())
    vtxR = np.hypot(vtx3[:,0], vtx3[:,1])
    vtxZ = vtx3[:,2]
    isFiducial = (vtxR<args.cutR) & (np.abs(vtxZ)<args.cutZ)

    nEventSelected = np.count_nonzero(isTagged & isFiducial)

    ## Keep the subrun number and the trigger ID numbers
    subRun = 0 if 'debug.r' not in fName else fName.split('.')[-2][1:].lstrip('0')
    subRun = 0 if subRun == '' else int(subRun)
    out_subRuns.append(np.ones(nEventSelected)*subRun)
    out_trigIDs.append(tree['TrigID'].array()[isTagged & isFiducial])

    ## Keep the other event information
    out_vtxRs.append(vtxR[isTagged & isFiducial])
    out_vtxZs.append(vtxZ[isTagged & isFiducial])

    ## Read FADC channels, first step is to determine the shape of the array
    fadc = np.array(tree['FADC'].array())
    if nT == 0: nT = fadc.shape[-1]
    if fadc.shape[1:] != (28, 8, nT): ## NOTE: shape of the FADC array is hardcoded, but I'll keep this numbers
        print("!!! Inconsistent array shape, ", fadc.shape)
        break

    ## split FADC by board/channels, 8 channels per boards
    fadc_hi = fadc[:, 1: 1+12].reshape([-1,96,nT]) ## High gain channels: 1-12
    fadc_lo = fadc[:,13:13+12].reshape([-1,96,nT]) ## Low gain channels: 13-24
    #fadc_Veto = fadc[:,25:25+3] ## veto channels: 25,26,27

    out_fadcs_lo.append(fadc_lo[isTagged & isFiducial])
    out_fadcs_hi.append(fadc_hi[isTagged & isFiducial])
    #out_fadcs_veto.append(fadc_veto)

## Join the output arrays
out_subRuns = np.concatenate(out_subRun)
out_trigIDs = np.concatenate(out_trigID)
out_vtxRs = np.concatenate(out_vtxR)
out_vtxZs = np.concatenate(out_vtxZ)
out_fadcs_lo = np.concatenate(out_fadc_lo)
out_fadcs_hi = np.concatenate(out_fadc_hi)

if not args.quiet:
    print("Low gain channel : shape=", fadc_lo.shape)
    print("                     min=", fadc_lo.min(), "max=", fadc_lo.max())
    print("High gain channel: shape=", fadc_hi.shape)
    print("                     min=", fadc_hi.min(), "max=", fadc_hi.max())
    #print("Veto channel     : shape=", fadc_veto.shape)
    #print("                     min=", fadc_veto.min(), "max=", fadc_veto.max())

if len(out_trigIDs) == 0:
    print("@@@ Empty output after event selection. skip writing output file")
    exit()

import h5py
kwargs = {'dtype':'f4', 'compression':'lzf'}
print("Saving output...", end="")
with h5py.File(args.output, 'w', libver='latest', swmr=True) as fout:
    m = fout.create_group('info')

    ## FADC_lo/hi array's original shape, to be used when restoring original shape in the user analysis
    ## NOTE: number of PMT channels is hard coded which is intended
    m.create_dataset('shape', data=[96, nT], dtype='i4')

    g = fout.create_group('events')
    g.create_dataset('subRun', data=out_subRuns, chunks=(1,), dtype='i4')
    g.create_dataset('trigID', data=out_trigIDs, chunks=(1,), dtype='i4')

    g.create_dataset('vtxR', data=out_vtxRs, chunks=(1,), dtype='f4')
    g.create_dataset('vtxZ', data=out_vtxZs, chunks=(1,), dtype='f4')

    g.create_dataset('fadc_lo', data=out_fadcs_lo, chunks=(1, 96, nT), **kwargs)
    g.create_dataset('fadc_hi', data=out_fadcs_hi, chunks=(1, 96, nT), **kwargs)

    print("")
    for name in ['subRun', 'trigID', 'vtxR', 'vtxZ', 'fadc_lo', 'fadc_hi']:
        print('>', name, g[name].shape)

print("Done.")
